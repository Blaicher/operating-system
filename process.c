#include "process.h"
#include "utils.h"
void startProcess(int procId, char* procName, unsigned int procPriority)
{
  process newProcess;
  newProcess.procId = procId;
  int ProcNameLength = strlen(procName);
  for(int j = 0; j < ProcNameLength; j++)
  {
    newProcess.procName[j] = procName[j];
  }
  newProcess.procPriority = procPriority;
  newProcess.isRunning = 1;
  processes[currentLocation] = newProcess;
  currentLocation++;
}

void killProcess(int procId)
{
  int locationOfArray = -1;
  for(int i = 0; i < currentLocation; i++)
  {
    if(!strcmp(processes[i].procId, procId))
    {
      // Delete process
      locationOfArray = i;
    }
  }
  if(locationOfArray != -1)
  {
      for(int i = locationOfArray; i < currentLocation - 1; i++)
      {
         processes[i] = processes[i + 1];
      }
      currentLocation--;
  }
}
int getProcessId(char* procName)
{
  for(int i = 0; i < currentLocation; i++){
    if(!strcmp(processes[i].procName, procName))
    {
      return processes[i].procId;
    }
  }
  return -1;
}
process* getProcessInformation(int procId)
{
  for(int i = 0; i < currentLocation; i++)
  {
    if(processes[i].procId == procId){
      return &processes[i];
    }
  }
  return NULL;
}
