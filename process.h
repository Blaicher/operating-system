#ifndef PROCESS_H
#define PROCESS_H

enum prioritys {STANDARD, MEDIUM, HIGH};
typedef struct process
{
    unsigned int procId;
    char procName[25];
    unsigned int procPriority;
    int isRunning;
} process;

process processes[3] = {0};
unsigned int currentLocation = 0;
void startProcess(int procId, char* procName, unsigned int procPriority);
void killProcess(int procId);
int getProcessId(char* procName);
process* getProcessInformation(int procId);
#endif // PROCESS_H
