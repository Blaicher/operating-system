#ifndef MEMORY_H
#define MEMORY_H
#include "types.h"
#define VGA_ADDRESS 0xB8000
void memread(void* address, int whence, char* buffer);
void memcpy(void* address, char** buffer, int whence);
void memwrite(void* address, char* buffer, unsigned int size);

#endif
