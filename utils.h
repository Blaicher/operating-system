#ifndef UTILS_H
#define UTILS_H
#include <stddef.h>
#include "types.h"

extern size_t strlen(const char*);
extern size_t digit_count(int);
extern void itoa(int, char *);
extern int atoi(char*);
extern uint32 strcmp(const char*, const char*);
extern uint32 start_with(const char*, const char*);
extern uint32 strstr(const char*, const char*);
extern uint32 isNum(char);
extern uint32 isLetter(char);
extern void strcpy(char*, char*);
int abs(int num);

#endif
