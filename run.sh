#assemble boot.s file
as --32 boot.s -o boot.o

#compile kernel.c file
gcc -m32 -c -lc -lgloss kernel.c -o kernel.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w

gcc -m32 -c utils.c -o utils.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w

gcc -m32 -c char.c -o char.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w

gcc -m32 -c process.c -o process.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w

gcc -m32 -c memory.c -o memory.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w

gcc -m32 -c FILE.c -o FILE.o -std=gnu99 -ffreestanding -O1 -Wall -Wextra -w
#linking the kernel with kernel.o and boot.o files
ld -m elf_i386 -T linker.ld kernel.o utils.o char.o process.o memory.o FILE.o boot.o -o BAPS.bin

#check BAPS.bin file is x86 multiboot file or not
grub-file --is-x86-multiboot BAPS.bin

#building the iso file
mkdir -p isodir/boot/grub
cp BAPS.bin isodir/boot/BAPS.bin
cp grub.cfg isodir/boot/grub/grub.cfg
grub-mkrescue -o BAPS.iso isodir

#run it in qemu
qemu-system-x86_64 -cdrom BAPS.iso
