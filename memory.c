#include "memory.h"
#define MAX_SIZE 130



void memread(void* address, int whence, char* buffer)
{
  if(whence > MAX_SIZE){
    return NULL;
  }
  int i = 0;
  for(i = 0; i < whence; i++){
    buffer[i] = *(char*)(address + i);
  }
  buffer[i] = '\0';

}

void memcpy(void* address, char** buffer, int whence)
{
  memread(address, whence, buffer);
}


void memwrite(void* address, char* buffer, unsigned int size)
{
  if (!strcmp(buffer, "FILL"))
  {
    for(unsigned int i = 0; i < size; i++){
      *(char*)(address + i) = '\0';
    }
    return;
  }
  for(unsigned int i = 0; i < size; i++)
  {
    *(char*)(address + i) = buffer[i];
  }
}
