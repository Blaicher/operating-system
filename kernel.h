#ifndef KERNEL_H
#define KERNEL_H
#include "types.h"
#include <stddef.h>
#define FALSE 0
#define TRUE !FALSE
#define VGA_ADDRESS 0xB8000
#define BUFSIZE 2200
#define MAX_VISIBLE_LINES 25
#define MAX_CHARS_IN_LINE 55
#define MAX_STRING_LENGTH 30
uint16* vga_buffer;

enum vga_color {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    GREY,
    DARK_GREY,
    BRIGHT_BLUE,
    BRIGHT_GREEN,
    BRIGHT_CYAN,
    BRIGHT_RED,
    BRIGHT_MAGENTA,
    YELLOW,
    WHITE,
    };

typedef struct pInput{
    char curInput[MAX_STRING_LENGTH];
    char allInput[MAX_VISIBLE_LINES][MAX_STRING_LENGTH];
    unsigned int inputIndex;
    unsigned int allInputSize;
    int currentViewedIndex;
}pInput;

void change_color(int color);
int backspace_char();
void baps_scanf(pInput* input);
void note_scanf();
void sleep(uint32 timer_count);
void wait_for_io(uint32 timer_count);
char get_input_keycode();
void outb(uint16 port, uint8 data);
void *sbrk(size_t incr);
uint8 inb(uint16 port);
void print_int(int num);
void baps_printf(char *str);
void print_char(char ch);
void print_new_line();
void init_vga(uint8 fore_color, uint8 back_color);
void clear_vga_buffer(uint16 **buffer, uint8 fore_color, uint8 back_color);
uint16 vga_entry(unsigned char ch, uint8 fore_color, uint8 back_color);
void moveMenu(char keycode);
void terminal(void);
void printMenu(void);
void getLine(char* str);
void getLineMenu(char* str);
void clearLine();
void clearLineT();
void startNotePad(char* fileName);
void getFiles();
void ls();

#include "keyboard.h"

#endif
