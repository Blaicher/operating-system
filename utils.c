#include "utils.h"

size_t strlen(const char* str)
{
    size_t length = 0;
    while(str[length])
        length++;
    return length;
}

size_t digit_count(int num)
{
    uint32 count = 0;
    if(num == 0)
        return 1;
    while(num > 0){
        count++;
        num = num/10;
    }
    return count;
}

uint32 strcmp(const char* first, const char* second)
{
    int firstLength = strlen(first);
    int secondLength = strlen(second);
    if(firstLength > secondLength)
    {
        return 1;
    }
    if(secondLength > firstLength)
    {
        return -1;
    }
    else // Strings have equal length, continue checking
    {
        for(int i = 0; i < firstLength; i++)
        {
            if(first[i] != second[i])
            {
                return 1;
            }
        }
        return 0; // Equal
    }

}

uint32 start_with(const char* first, const char* second) // Function will copy a string to another string
{
    if(!strcmp(first, second)) // Strings are the same
    {
        return 1;
    }
    if(strlen(second) >= strlen(first)) // If they are the same length or bigger it's not possible for first to start with second
    {
        return 0;
    }
    for(uint32 i = 0; i < strlen(second); i++)
    {
        if(second[i] != first[i])
        {
            return 0;
        }

    }
    return 1;
}
uint32 strstr(const char* hey, const char* hatch)
{
    /*
     Function will search for a substring inside the main string.
     Input:
        const char* hey - a string to search inside
        const char* hatch - the string to search in
     Output:
        True if substring exists inside main string otherwise False
      */
    unsigned int j = 0;
    unsigned int i = 0;
    unsigned int heylen = strlen(hey);
    unsigned int hatchlen = strlen(hatch);
    for(i = 0; i < hatchlen; i++)
    {
        if(j == heylen) // We have reached the end of the string and managed to find all characters
        {
            return 1;
        }
        if(hatch[i] != hey[j]) // Not yet
        {
            j = 0;
        }
        else
        {
            j++;
        }
    }
    // Couldn't find
    return 0;
}


void itoa(int num, char *number)
{
    int dgcount = digit_count(num);
    int index = dgcount - 1;
    char x;
    if(num == 0 && dgcount == 1){
        number[0] = '0';
        number[1] = '\0';
    }else
    {
        while(num != 0){
            x = num % 10;
            number[index] = x + '0';
            index--;
            num = num / 10;
        }
        number[dgcount] = '\0';
    }
}

int abs(int num){
    if (num < 0)
    {
        num -= num * 2;
    }
    return num;
}

uint32 isNum(char chr)
{
    if((chr >= '0' && chr <= '9'))
    {
        return 1;
    }
    return 0;
}

uint32 isLetter(char chr)
{
    if((chr >= 'A' && chr <= 'Z'))
    {
        return 1;
    }
    return 0;
}
/*
 * Function will copy a string to another string
 * Input:
 *  src - String to copy
 *  dest - Destination to copy to
 * Output: Void
 */
void strcpy(char* src, char* dest)
{
    unsigned int len = strlen(src); // Get source's length
    unsigned int i = 0;
    for(i = 0; i < len; i++) // Run of string's length
    {
        dest[i] = src[i]; // Copy character one by one
    }
    dest[i] = '\0'; // Making sure string is null-terminated
}


int atoi(char* str)
{
  int len = strlen(str); // Getting length of String
  int num = 0; // Output number

  for(int i = 0; i < len; i++)
  {
    num *= 10;
    num += (int)(str[i] - '0'); // 456
  }
  return num;
}
