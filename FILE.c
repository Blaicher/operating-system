#include "FILE.h"
#include "types.h"
#include "memory.h"
#include "utils.h"
#define MISSING_PERMISSION_ERROR -2
#define FILE_NOT_FOUND -1
#define MEMORY_EXCEEDED -3
#define NULL 0
#define IS_ERROR_LOCATION 0x420
#define FILL "FILL"
unsigned short* Memstart = (unsigned short*)(FILE_MEMORY_START);


char getc(FILE* filePointer)
{
    if (filePointer->address == NULL|| filePointer->loc >= 100)
    {
      return NULL;
    }
    char filec = filePointer->fileContent[filePointer->loc];
    filePointer->loc++;
    return filec;
}

int putc(FILE* filePointer)
{
  if (filePointer->address == NULL || filePointer->loc >= 100)
  {
    return -1;
  }
  filePointer->fileContent[filePointer->loc];
  filePointer->loc++;
}


void fopen(char* fileName, unsigned int permission, FILE* file)
{
    int fileNameLen = strlen(fileName);
    void* addressToFile;
    int address = doesFileExist(fileName);
    addressToFile = (void*)address;
    if(addressToFile == NULL)
    {
      char a[] = "error";
      void* memstart1 = (void*)0x14000;
      memwrite(memstart1, a, strlen(a));
      return;
    }

    file->address = addressToFile;
    for(int i = 0; i < fileNameLen; i++)
    {
      file->fileName[i] = fileName[i];
    }
    file->fileName[fileNameLen] = '\0';
    // Getting file content and remaining information
    char strfileSize[5];
    memread(file->address + 100, 4, strfileSize);
    file->fileSize = atoi(strfileSize);
    for(int i = 0; i < file->fileSize; i++)
    {
      file->fileContent[i] = (char)(file->address + 104 + i);
    }
    file->fileContent[file->fileSize] = '\0';
    file->permission = permission;
    file->loc = 0;
    void* memstart1 = (void*)0x14000;
    memwrite(memstart1, strfileSize, strlen(strfileSize));

}

int fseek(FILE* filePointer, int offset, int whence)
{
  if(offset + whence < 100)
  {
    return MEMORY_EXCEEDED;
  }
  filePointer->loc = offset + whence;
}

int fwrite(FILE* filePointer, char* content)
{
  if(filePointer->permission == BAPS_READ)
  {
    return MISSING_PERMISSION_ERROR;
  }
  if(filePointer->address == NULL)
  {
    return FILE_NOT_FOUND;
  }
  if(filePointer->loc + strlen(content) >= 100)
  {
    return MEMORY_EXCEEDED;
  }
  memwrite(filePointer->address + 204 + filePointer->loc, content, strlen(content));
  return 0;
}

char* fread(FILE* filePointer, int offset, int whence)
{
  if(filePointer->permission == BAPS_WRITE || filePointer->address == NULL || whence + offset >= 100)
  {
    return NULL;
  }
  // Read
  char fileContent[100] = {0};
  int j = 0;
  for(int i = offset; i < offset + whence; i++)
  {
    fileContent[j] = filePointer->fileContent[i];
    j++;
  }
  fileContent[j + 1] = '\0';
  return fileContent;
}

int doesFileExist(char* fileName){
  void* memtemp = (void*)FILE_MEMORY_START;
  unsigned int fileNameLen = strlen(fileName);
  size_t memloc = FILE_MEMORY_START;
  while(memtemp < FILE_MEMORY_START + sizeof(FILE) * 10)
  {
    char name[100] = {0};
    memread(memtemp, 100, name);
    if(!strcmp(name, fileName))
    {
      // Found the file
      return memloc;
    }
    memtemp += sizeof(FILE);
    memloc += sizeof(FILE);
  }
  return NULL;
}



int createFile(char* fileName)
{
  void* memtemp = (void*)Memstart;
  while(memtemp < (void*)Memstart + sizeof(FILE) * 10)
  {
    // Check if a file exists in the location
    char information[100] = {0};
    memread((void*)memtemp, 100, information); // Reading info into array
    if (information[0] == 'B')
    {
      // File exists in location.
      memtemp = memtemp + sizeof(FILE);
      continue;
    }
    // Found a spot for the file
    memwrite((void*)memtemp, fileName, strlen(fileName));
    memwrite((void*)memtemp + strlen(fileName), FILL, 100 - strlen(fileName));
    char size[4] = "0000";
    memwrite((void*)memtemp + 100, size, strlen(size));
    memwrite((void*)memtemp + 100 + strlen(size), FILL, 100);
    return 1;
  }
  return 0;
}

void showFiles()
{
  void* memtemp = (void*)Memstart;
  void* toWrite = (void*)0x13000;;
  while(memtemp < FILE_MEMORY_START + sizeof(FILE) * 10)
  {

    char information[100] = {0};
    memread((void*)memtemp, 100, information); // Reading file Name
    if(information[0] == 'B')
    {
    memwrite(toWrite, information, 100);
    toWrite = toWrite + 0x100;
  }
    memtemp += sizeof(FILE);
  }
}

int editFile(FILE* file, char* newContent)
{
  if(file->address == NULL)
  {
    return -1;
  }
  strcpy(newContent, file->fileContent);
  file->fileSize = strlen(newContent);
}
