#include "kernel.h"
#include "utils.h"
#include "char.h"
#include "FILE.h"
#include "memory.h"
#define MAX_STRING_LENGTH 30
#define LINE_LENGTH 80
uint32 vga_index;
static uint32 next_line_index = 1;
uint8 g_fore_color = WHITE, g_back_color = BLACK;
int digit_ascii_codes[10] = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};
int capsLockOn = FALSE;
char files[5][100] = {0};
void kernel_entry()
{
    init_vga(WHITE, BLACK);
    printMenu();
    char a[] = "BAfiletest";
    char bd[] = "Brock";
    FILE pfile = {0};
    void* memstart1 = (void*)0x13000;
    print_int(createFile(a));
    print_int(createFile(bd));

    char str[MAX_CHARS_IN_LINE];
    char keycode = 0;
    do{
        keycode = get_input_keycode();
        if(keycode == KEY_UP || keycode == KEY_DOWN)
        {
            unsigned int temp = vga_index; // Keep original value
            unsigned int temp_line = next_line_index;
            if(keycode == KEY_UP)
            {
                vga_index -= LINE_LENGTH; // Check line above
                next_line_index--;
            }
            else
            {
                vga_index += LINE_LENGTH; // Checking line below
                next_line_index++;
            }
            getLineMenu(str);
            vga_index = temp; // Return to original value
            next_line_index = temp_line;
            if(!strstr("--", str))
            {
                moveMenu(keycode);
            }
        }
        if(keycode == KEY_ENTER)
        {
            getLineMenu(str);
            if(!strcmp(str, "Terminal"))
            {
                terminal();
            }
        }
        sleep(0x02FFFFFF); // Sleep to give user enough time to let go off the key
        sleep(0x02FFFFFF); // Sleep to give user enough time to let go off the key
        sleep(0x04FFFFFF); // Sleep to give user enough time to let go off the key

    }while(TRUE);


}
void terminal(void)
{
    unsigned int i = 0;
    clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
    pInput* input;
    input->inputIndex = 0;
    input->currentViewedIndex = 1;
    input->allInputSize = 0;
    do
    {
        baps_printf("BAPS~>");
        get_input_keycode();
        baps_scanf(input);
        strcpy(input->curInput, input->allInput[input->allInputSize - 1]);
        input->allInputSize++;
        if(!strcmp(input->curInput, "exit"))
        {
            break;
        }
        if(!strcmp(input->curInput, "help"))
        {
            clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
            baps_printf("Current options:\n");
            baps_printf("[1]\tcolor\n");
            baps_printf("[2]\tclear\n");
            baps_printf("[3]\texit\n");
            baps_printf("for more information about the commands write help {command}\n");
        }
        else if(!strcmp(input->curInput, "help color"))
        {
            baps_printf("color - function to change color of text:\n\ncolor a - GREEN\ncolor b - BLUE\n");
        }
        else if(start_with(input->curInput, "color"))
        {
            if(!strcmp(input->curInput, "color a"))
            {
                change_color(GREEN);
            }
            else if(!strcmp("color p", input->curInput))
            {
                baps_printf("\n");
                print_int(input->allInputSize);
            }
            else
            {
                baps_printf("Missing operand!\nUsage: color {color}\n    Ex: color a\n");
            }
        }
        else if(!strcmp(input->curInput, "clear"))
        {
            clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
        }
        else if(!strcmp(input->curInput, "check"))
        {
            for(unsigned int i = 0; i < input->allInputSize; i++)
            {
                baps_printf(input->allInput[i]);
                baps_printf("\n");
            }
        }
        else if(!strcmp(input->curInput, "ls"))
        {
          ls();
        }
        else if(start_with(input->curInput, "touch "))
        {
          int i = strlen("touch ");
          print_int(i);
          int isLegal = 1;
          char name[100] = {0};
          name[0] = 'B';
          int j = 1;
          while(i < strlen(input->curInput))
          {
            if(input->curInput[i] == ' ')
            {
              baps_printf("Illegal file name\n");
              isLegal = 0;
              continue;
            }
            name[j] = input->curInput[i];
            i++;
            j++;
          }
          if(isLegal != 0)
          {
            // Got file name and is valid
            int res = createFile(name);
            if(res != 1)
            {
              baps_printf("Error creating file\n");
            }
          }

        }
        else if(i != 0)
        {
            baps_printf("Unknown command!\nUse help to get existing commands\n");
        }



        else
        {
            clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
            i++;
        }

        sleep(0x02FFFFFF); // Sleep to give user enough time to let go off the key
        sleep(0x02FFFFFF); // Sleep to give user enough time to let go off the key
        sleep(0x05FFFFFF); // Sleep to give user enough time to let go off the key

    }while(TRUE);
    baps_printf("Exiting...");
    return;
}
uint16 vga_entry(unsigned char ch, uint8 fore_color, uint8 back_color)
{
    uint16 ax = 0;
    uint8 ah = 0, al = 0;

    ah = back_color;
    ah <<= 4;
    ah |= fore_color;
    ax = ah;
    ax <<= 8;
    al = ch;
    ax |= al;

    return ax;
}

void clear_vga_buffer(uint16 **buffer, uint8 fore_color, uint8 back_color)
{
    uint32 i;
    for(i = 0; i < BUFSIZE; i++){
        (*buffer)[i] = vga_entry(NULL, fore_color, back_color);
    }
    next_line_index = 1;
    vga_index = 0;
}

void init_vga(uint8 fore_color, uint8 back_color)
{
    vga_buffer = (uint16*)VGA_ADDRESS;
    clear_vga_buffer(&vga_buffer, fore_color, back_color);
    g_fore_color = fore_color;
    g_back_color = back_color;
}

void print_new_line(){

    if(next_line_index >= MAX_CHARS_IN_LINE){
        next_line_index = 0;
        clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
    }
    vga_index = LINE_LENGTH*next_line_index;
    next_line_index++;
}

void print_char(char ch)
{
    vga_buffer[vga_index] = vga_entry(ch, g_fore_color, g_back_color);
    vga_index++;
}
int backspace_char()
{
    /*
     Function implements backspace by deleting the putting null in the last buffer location
     (last char entered) and makes it the new current index, so we will override it with the next entry
     Input: void
     Output: int - 1 if character was deleted, otherwise 0
    */
    char ch = vga_buffer[vga_index - 1]; // Get last char in order to not override needed chars in terminal
    if(ch != '>') // Don't override username in terminal!
    {
        vga_index--;
        vga_buffer[vga_index] = vga_entry(NULL, g_fore_color, g_back_color);
        return TRUE;
    }
    return FALSE;
}

void change_color(int color)
{
    /*
    Run over the buffer and rewrite it with the new color requested!
    Input: int color - color requested
    Output: void
    */
    unsigned int i = 0;
    g_fore_color = color;
    for(i = 0; i < vga_index; i++) // Running over the buffer
    {
        vga_buffer[i] = vga_entry(vga_buffer[i], g_fore_color, g_back_color); // Switching current character with himself but with different color
    }

}
void baps_printf(char* str)
{
    /*
     * Function will receive a string and write it to screen (supports \n - new line \t - tab)
     * Input: string to print to the screen
     * Output: void
     */
    uint32 index = 0;
    while(str[index]){
        if(str[index] == '\n') // New line
        {
            print_new_line();
        }
        else if(str[index] == '\t') // Tab
        {
            for(unsigned int i = 0; i < 4; i++)
            {
                print_char(' ');
            }
        }
        else
        {
            print_char(str[index]);
        }
        index++;
    }
}

void print_int(int num)
{
    /*
     * Function will receive an int, use the function itoa to make it a null terminated string. and call baps_printf with it
     * Input: int num - Number to print
     * Output: void
    */
    char str_num[digit_count(num)+1]; // Make room for string
    itoa(num, str_num); // Transform int to string
    baps_printf(str_num); // Print it
}

uint8 inb(uint16 port)
{
    uint8 ret;
    asm volatile("inb %1, %0" : "=a"(ret) : "d"(port));
    return ret;
}

void outb(uint16 port, uint8 data)
{
    asm volatile("outb %0, %1" : "=a"(data) : "d"(port));
}

char get_input_keycode()
{
    /*
     * Function will get the keycode input from the keyboard and will return it
     * Input: void
     * Output: char representing the keycode entered
     */
    char ch = 0;
    while((ch = inb(KEYBOARD_PORT)) != 0){
        if(ch > 0)
            return ch;
    }
    return ch;
}


void wait_for_io(uint32 timer_count)
{
    while(1){
        asm volatile("nop");
        timer_count--;
        if(timer_count <= 0)
            break;
    }
}

void sleep(uint32 timer_count)
{
    wait_for_io(timer_count);
}

void baps_scanf(pInput* input)
{

    char ch = 0;
    char keycode = 0;
    unsigned int i = 0;
    char curr[MAX_STRING_LENGTH] = {0};
    input->currentViewedIndex = input->allInputSize - 1;
    do {
        keycode = get_input_keycode();
        if(keycode == KEY_ENTER) // Input has been received
        {

            print_new_line();
            input->curInput[i] = '\0';
            return; // return the str
        }
        ch = get_ascii_char(keycode);
        if(keycode == KEY_CAPSLOCK)
        {
          capsLockOn = !capsLockOn;
          continue;
        }
        else if(keycode == KEY_BACKSPACE)
        {
            if(backspace_char())
            {
                i--; // Making sure the character is deleted
            }
        }
        else if(capsLockOn == FALSE && isLetter(ch))
        {
            ch = toLower(ch);
        }
        else if(keycode == KEY_UP || keycode == KEY_DOWN)
        {
            strcpy(input->curInput, curr);
            if(keycode == KEY_UP && input->currentViewedIndex - 1 >= 0)
            {
                input->currentViewedIndex--;
                vga_index -= i; // Overriding string
                clearLineT(); // Clearing current line
                i = 0;
                unsigned int len = strlen(input->allInput[input->currentViewedIndex]); // Getting length of string to copy
                for(unsigned int j = 0; j < len; j++)
                {
                    input->curInput[j] = input->allInput[input->currentViewedIndex][j];
                    vga_buffer[vga_index] = vga_entry(input->curInput[j], g_fore_color, g_back_color);
                    vga_index++;
                    i++;
                }
            }
            else if (input->currentViewedIndex + 1 < input->allInputSize - 1 && keycode == KEY_DOWN)
            {
                input->currentViewedIndex++;
                vga_index = vga_index - (vga_index % LINE_LENGTH) + 6; // Getting to start of line
                unsigned int len = strlen(input->allInput[input->currentViewedIndex]); // Getting length of new string
                clearLineT();
                // Adding the new input
                int i = 0;
                for(unsigned int j = 0; j < len; j++) // Writing the new string
                {
                    input->curInput[j] = input->allInput[input->currentViewedIndex][j];
                    vga_buffer[vga_index] = vga_entry(input->curInput[j], g_fore_color, g_back_color);
                    vga_index++;
                    i++;
                }
            }
             else if(keycode == KEY_DOWN)
             {
                 clearLineT();
                 vga_index = vga_index - (vga_index % LINE_LENGTH) + 6; // Getting to start of line
                 size_t len = strlen(input->curInput);
                 for(size_t j = 0; j < len; j++)
                 {
                     input->curInput[j] = NULL;
                 }
                 i = 0;

             }
            }



        if(keycode != KEY_MINUS && keycode != KEY_BACKSPACE && keycode != KEY_DOWN && keycode != KEY_UP)
        {
            print_char(ch); // Show entered key
            input->curInput[i] = ch; // Get into string
            i++;
        }
        sleep(0x06FFFFFF); // Sleep to give user enough time to let go off the key


    }while(ch != KEY_ENTER);
    input->curInput[i] = '\0';
    strcpy(input->curInput, input->allInput[input->inputIndex]);
    input->inputIndex++;
    return;
}


void moveMenu(char keycode)
{
    /*
     Function will receive a keycode which is either arrow up or arrow down
     and move the visible lines accordingly knowing that the maximum possible visible lines is 25
     */
    unsigned int i = 0;
    unsigned int f = 0;
    // Determining the direction:
    if(keycode == KEY_UP || keycode == KEY_DOWN)
    {
        if(keycode == KEY_UP)
        {
            f = LINE_LENGTH;
        }
        else
        {
            f = -LINE_LENGTH;
        }
        unsigned int eol = vga_index - (vga_index % LINE_LENGTH) + MAX_CHARS_IN_LINE - 6; // Getting the end of line index
        for(i = vga_index - (vga_index % LINE_LENGTH) + 5; i < eol; i++) // Running from start to end of line
        {
            vga_buffer[i] = vga_entry(vga_buffer[i], g_fore_color, g_back_color);
        }
        vga_index -= f;
        eol = vga_index - (vga_index % LINE_LENGTH) + MAX_CHARS_IN_LINE;
        for(i = vga_index - (vga_index % LINE_LENGTH) + 5; i < eol - 6; i++)
        {
            vga_buffer[i] = vga_entry(vga_buffer[i], BLACK, RED);
        }
    }

}

void printMenu(void)
{
    unsigned int needed_index = 0;
    unsigned int needed_line = 0;
    unsigned int i = 0;
    baps_printf("\n\tChoose an option\n\n");
    baps_printf("\t");
    for(i = 0; i < 46; i++)
    {
        print_char('-');
    }
    baps_printf("\n");
    needed_index = vga_index + 6;
    needed_line = next_line_index;
    for(i = 0; i < 2; i++)
    {
        vga_index += 49;
        vga_buffer[vga_index] = vga_entry('|', g_fore_color, g_back_color);
        vga_index -= 49;
        baps_printf("\t|\n");

    }
    baps_printf("\t");
    for(i = 0; i < 46; i++)
    {
        print_char('-');
    }
    vga_index = needed_index;
    next_line_index = needed_line;
    baps_printf("Terminal");
    vga_index += 80 - (vga_index % 80 - 6);
    baps_printf("Hello!");


}

void getLine(char* str)
{
    unsigned int eol = vga_index - (vga_index % LINE_LENGTH) + MAX_CHARS_IN_LINE;
    unsigned int sol = vga_index - (vga_index % LINE_LENGTH);
    unsigned int j = 0;
    for(unsigned int i = sol; i < eol; i++)
    {

        if(vga_buffer[i] != NULL)
        {
            print_char(vga_buffer[i]);
            str[j] = vga_buffer[i];
            j++;
        }
    }

}

void getLineMenu(char* str)
{
    unsigned int eol = vga_index - (vga_index % LINE_LENGTH) + MAX_CHARS_IN_LINE;
    unsigned int sol = vga_index - (vga_index % LINE_LENGTH);
    unsigned int j = 0;
    for(unsigned int i = sol; i < eol; i++)
    {
        char ch = vga_buffer[i];
        if(ch != ' ' && ch != NULL && ch != '|')
        {
            str[j] = ch;
            j++;
        }
    }
    str[j] = '\0';
}

void clearLine()
{
    unsigned int sol = vga_index - (vga_index % LINE_LENGTH); // Getting start of line
    for(int j = 0; j < MAX_CHARS_IN_LINE; j++)
    {
        vga_buffer[sol] = vga_entry(NULL, g_fore_color, g_back_color); // Overriding characters one by one
        sol++;
    }
}

void clearLineT()
{
    unsigned int sol = vga_index - (vga_index % LINE_LENGTH) + 6; // Getting start of line
    for(int j = 0; j < MAX_CHARS_IN_LINE; j++)
    {
        vga_buffer[sol] = vga_entry(NULL, g_fore_color, g_back_color); // Overriding characters one by one
        sol++;
    }
}

void reWriteLine(char* newLine)
{
    vga_index = vga_index - (vga_index % LINE_LENGTH) + 6;
    clearLineT();
    size_t len = strlen(newLine);
    for(size_t j = 0; j < len; j++)
    {
        vga_buffer[vga_index] = vga_entry(newLine[j], g_fore_color, g_back_color);
        vga_index++;
    }
}


void getFiles()
{
  // Using filesystem function
  showFiles();
  // Reading from location wrote to
  void* filesLocation = (void*)0x13000;
  for(int i = 0; i < 5; i++)
  {
    memread(filesLocation, FILE_NAME_SIZE, &(*files[i]));
    filesLocation = filesLocation + 0x100;
  }
}

void ls()
{
  getFiles();
  for(int i = 0; i < 5; i++)
  {
    if(files[i][0] == 'B')
    {
      char curr[100] = {0};
      for(int j = 0; j < strlen(files[i]) - 1; j++)
      {
        curr[j] = files[i][j + 1];
      }

      baps_printf(curr);
      if(files[i + 1][0] == 'B'){
      baps_printf(", ");
      }
    }
  }
  baps_printf("\n");
}


void fedit(char* fileName)
{
  FILE file = {0};
  fopen(fileName, BAPS_READ_WRITE, &file);
  if(file == NULL)
  {
    baps_printf("Couldn't find file specified\n");
    return;
  }
  editFile(&file);
}

void startNotePad(char* fileName)
{
  FILE file = {0};
  fopen(fileName, BAPS_READ_WRITE, &file);
  if(file == NULL)
  {
    baps_printf("Couldn't find file specified\n");
    return;
  }
  clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
  baps_printf(file.fileContent);
  baps_scanf();
}
