#ifndef FILE_H
#define FILE_H
#define BAPS_READ_WRITE 0
#define BAPS_READ 1
#define BAPS_WRITE 2
enum fileInfo {FILE_NAME_SIZE = 100, FILE_SIZE_BYTES = 3, INDICATION_SIZE = 6};

typedef struct FILE{
  void* address;
  char fileName[FILE_NAME_SIZE]; // Name of file
  int fileSize; // size of file in bytes
  char fileContent[100]; // Containing file content
  unsigned int loc;
  unsigned int permission;
}FILE;


char getc(FILE* filePointer);
int putc(FILE* filePointer);
void fopen(char* fileName, unsigned int permission, FILE* file); // Open a file
int createFile(char* fileName);
int fseek(FILE* filePointer, int offset, int whence); // Move in file
int fwrite(FILE* filePointer, char* content); // Write to file
char* fread(FILE* filePointer, int offset, int whence); // Read from file
int saveFile(char* fileName, int newFileSize);
int deleteFile(char* fileName);
int doesFileExist(char* fileName);
int editFile(FILE* file, char* newContent);
int renameFile(char* fileName, char* newFileName);
void showFiles();
#endif // FILE_H
